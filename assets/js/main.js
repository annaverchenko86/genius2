﻿ // autoplay video
                   function onPlayerReady(event) {
                       event.target.playVideo();
                   }
                   // when video ends
                   function onPlayerStateChange(event) {
                       if(event.data === 0) {
                           document.getElementById('welcomeDiv').style.zIndex = 1000;;
                       }
                   }
                   function onYouTubePlayerAPIReady() {
                   (function ($) {

                       $(document).ready(function() {


                           $(".wathvideo, .fancy-youtube").fancybox({
                               padding:0,
                               width: 640,
                               height: 390,
                               beforeShow  : function() {
                                   var  player = new YT.Player('player', {
                                       height: '390',
                                       width: '640',
                                       videoId: 'MUDoed796k4',
                                       events: {
                                           
                                           'onStateChange': onPlayerStateChange
                                       }
                                   });
                               }
                           });
                       });
                   })(jQuery);
                   }
				   
(function ($) {
	

    (function ($) {
	
        $.fn.equalHeight = function () {
            var group = this;
            $(window).bind('resize',function () {
                var tallest = 0;
                $(group).height('auto').each(function () {
                    tallest = Math.max(tallest, $(this).height());
                }).height(tallest);
            }).trigger('resize');
        }
    })(jQuery)
    var panelIsVisible = true;
    var headerBlockHeight = 59;
    var HeaderBlockFullHeight = 120;

    var closeTimeOut = null;
    var secondMenuPanel = null;

    function panelHide(panel, headerblock, offsetTop, panelHeight, speed) {
        panelIsVisible = false;
        panel.stop();

        headerblock.stop();
 	//headerblock.animate({
       //     height:headerBlockHeight
       // }, speed);
        

        panel.animate({
            marginTop:'-=' + parseInt(offsetTop + panelHeight) + 'px'
        }, speed, function () {
            panel.hide();
        });
    }

    function panelShow(panel, headerblock, offsetTop, headerblockHeight, speed) {
        panelIsVisible = true;
        panel.stop();
        headerblock.stop();
        panel.css('marginTop', -5);
        panel.show();
        panel.animate({
            marginTop: offsetTop + 'px'
        }, speed);
      // headerblock.animate({
      //      height:HeaderBlockFullHeight + 'px'
      //  }, 2 * speed);
    }

    function HideShowPanel(height, speed) {
        var items = $('#headerNav .topNavigation ul.menu>li');
        var secondMenuExist = $('#headerBlock').hasClass('withsecondlevel');
        panel = $('.secondLevel, .secondLevelhide');
        panelIsVisible = panel.is(':visible');
        var panelHeight = panel.height();
        var headerblock = panel.parent();
        var hoverblock = $('#headerNav');
        if ($('body').hasClass('admin-menu')) {
            var corect = 0;
        }
        else {
            var corect = 0;
        }

        var headerblockHeight = $('#headerBlock').height();
        var offsetTop = hoverblock.height() + corect;
        if (secondMenuExist) {
            $(window).scroll(function () {

                if ((($(window).scrollTop() > height) && panelIsVisible)) {
                    panelHide($('.secondLevel'), headerblock, offsetTop, panelHeight, speed, 0);
                }
                if (($(window).scrollTop() <= height) && !panelIsVisible) {
                    panelShow($('.secondLevel'), headerblock, offsetTop, headerblockHeight, speed)
                }
            });
        }


        items = $('#headerNav .topNavigation ul.menu>li');

        headerblock.hover(function (e) {
	    if (closeTimeOut) {
            	window.clearTimeout(closeTimeOut);
                closeTimeOut = null;
            }		
            if (secondMenuExist && !$('.secondLevel').is(':visible')) {
                panelShow($('.secondLevel'), headerblock, offsetTop, headerblockHeight, speed)
            }
        }, function() {
            if (secondMenuExist && $('.secondLevel').is(':visible') && $(window).scrollTop() > height) {
	    	if (closeTimeOut) {
            		window.clearTimeout(closeTimeOut);
                	closeTimeOut = null;
            	}
            	closeTimeOut = window.setTimeout(function () {
            		panelHide($('.secondLevel'), headerblock, offsetTop, panelHeight, speed, 0);
	    	},500);	
            }
        });

/*

        headerblock.hover(function (e) {
            li = $(e.target).parents('li');

            panelIsVisible = $('.secondLevel:visible, .secondLevelhide:visible').length;
            if (!panelIsVisible) {
                if (li.length > 0) {
                    var arrClass = $(li).attr('class').split(' ');
                    var menuId = null;
                    for (var i in arrClass) {
                        if (arrClass[i].indexOf('menu-mlid-') > -1) {
                            menuId = arrClass[i].substr(4);
                            break;
                        }
                    }

                    var secondMenuClass = $('.parent' + menuId).parents('.block-wrapper');

                    if (secondMenuClass.length > 0) {
                        $('.block-wrapper', '.secondLevelhide').hide();
                        secondMenuClass.show();
                        if (closeTimeOut) {
                            window.clearTimeout(closeTimeOut);
                            closeTimeOut = null;
                        }
                        panelShow($('.secondLevelhide'), headerblock, offsetTop, headerblockHeight, speed);
                    }

                } else {
                    if (secondMenuExist) {
                        if (closeTimeOut) {
                            window.clearTimeout(closeTimeOut);
                            closeTimeOut = null;
                        }
                        panelShow($('.secondLevel'), headerblock, offsetTop, headerblockHeight, speed);
                    }
                }

            }
        }, function (e) {
            panelIsVisible = $('.secondLevel:visible, .secondLevelhide:visible').length;
            if (panelIsVisible) {
                if (closeTimeOut) {
                    window.clearTimeout(closeTimeOut);
                    closeTimeOut = null;
                }
                closeTimeOut = window.setTimeout(function () {
                    if ($(window).scrollTop() > height || !secondMenuExist) {
                        if ($('.secondLevel').is(':visible')) {
                            panelHide($('.secondLevel'), headerblock, offsetTop, panelHeight, speed);
                        } else {
                            panelHide($('.secondLevelhide'), headerblock, offsetTop, panelHeight, speed);
                        }
                    } else {
                        if (secondMenuExist) {
                            $('.secondLevelhide').hide();
                            $('.secondLevel').show();
                        }
                    }
                    if ($('.secondLevel').is(':visible') && $(window).scrollTop() > height) {
                        panelHide($('.secondLevel'), headerblock, offsetTop, panelHeight, speed);
                    } else if ($('.secondLevelhide').is(':visible') && $(window).scrollTop() > height) {
                        panelHide($('.secondLevelhide'), headerblock, offsetTop, panelHeight, speed);
                    } else if (secondMenuExist && $(window).scrollTop() <= height) {
                        $('.secondLevelhide').hide();
                        $('.secondLevel').show();
                    }
                }, 2000);

            }
            items.removeClass('hover_item');
        });

        items.mouseover(function () {
            if ($(this).hasClass('hover_item')) return;
            items.removeClass('hover_item');
            $(this).addClass('hover_item');
            li = $('.menu-level-1 .menu li.active');
            var arrClass = $(li).attr('class').split(' ');
            var menuId = null;
            for (var i in arrClass) {
                if (arrClass[i].indexOf('menu-mlid-') > -1) {
                    menuId = arrClass[i].substr(4);
                    break;
                }
            }
            var secondMenuClass = $('.parent' + menuId).parents('.block-wrapper');
            if (secondMenuClass.length > 0) {
                panelIsVisible = $('.secondLevel:visible, .secondLevelhide:visible').length;
                if (panelIsVisible) {
                    $('.secondLevel').hide();
                    $('.block-wrapper', '.secondLevelhide').hide();
                    secondMenuClass.show();
                    $('.secondLevelhide').css('marginTop', '59px');
                    $('.secondLevelhide').show();
                } else {
                    $('.block-wrapper', '.secondLevelhide').hide();
                    secondMenuClass.show();
                    if (closeTimeOut) {
                        window.clearTimeout(closeTimeOut);
                        closeTimeOut = null;
                    }
                    panelShow($('.secondLevelhide'), headerblock, offsetTop, headerblockHeight, speed);
                }

            }
        });
*/

    }

    jQuery(document).ready(function () {
		jQuery('.videolink').fancybox({
			tpl: {
			  // wrap template with custom inner DIV: the empty player container
			  wrap: '<div class="fancybox-wrap forvideo" tabIndex="-1" >' +
					'<div class="fancybox-skin">' +
					'<div class="fancybox-outer">' +
					'<div id="player1">' + // player container replaces fancybox-inner
					'</div></div></div></div>' 
			},
		 
			beforeShow: function () {
			 var base = this.href.slice(1),
			 cdn = "";
		 
			  // install player into empty container
			  jQuery("#player1").flowplayer({
				splash: true,
				//engine : 'flash',
				ratio: 9/16,
				swf: "/sites/all/themes/genius/js/flowplayer/flowplayer.swf",
				playlist: [
				  [
					{ mp4: cdn + base },
				  ]
				]
			  });
			  //flowplayer("#player").play(0);
		 
			},
			beforeClose: function () {
			  // important! unload the player
			  flowplayer("#player1").unload();
			},
		});
	
        jQuery(".ibm_featur a").attr('target', '_blank');
        jQuery(".swfpopup").fancybox({
			padding: 2,
			width: 1000,
			maxHeight: 750,
		});
        jQuery(".iframepopup").fancybox({
			padding:2,
			type:'iframe',
			width:990,
			maxHeight:556,
		});
		jQuery(".autoopenpopup").click();
		
        $('.chatLink').click(function (event) {
            event.preventDefault();
            parent.LC_API.open_chat_window();
        })

          $('.featur-item div.feature-subitem.bigit:nth-child(2n)').addClass('clearer');
 
		//Add quotes to testimonials
		$('.blocktestimonial .field-name-body p, .field-name-field-customer-testimonials .field-name-body p').prepend(" <span class='bqstart'></span> ");
		$('.blocktestimonial .field-name-body p, .field-name-field-customer-testimonials .field-name-body p').append(" <span>&nbsp;</span><span class='bqend'></span> ");
		
        var headerBlock = $('#headerBlock');
        var animationSpeed = 800; // швидкість анімації
        var heightForBeginAnimation = 100; // висота(величчина скролу) при якій почнеться анімація
        secondMenuPanel = $('.secondLevel'); // панель яка буде ховатись

        var secondMenuWrapper = $('#headerBlock .secondLevelhide');
        $('.greyarrow:even').addClass('even');
        $('.greyarrow:odd').addClass('odd');

        $('.block-wrapper', secondMenuWrapper).hide();
        secondMenuWrapper.hide();
        HideShowPanel(heightForBeginAnimation, animationSpeed);


        var footerheight = $('#footer').height();
        $('#content2').css({'min-height':(($('body').height()) - footerheight - 110) + 'px'});
        $(window).resize(function () {
            $('#content2').css({'min-height':(($('body').height()) - footerheight - 110) + 'px'});
        });

        var count_visible_li = 4;
        var ul = $('.footerColumn1 #block-menu-menu-other-popular-searches .content  ul.menu');
        var lis = $('li', ul);
        ul.css('height', '100%');
        var ul_all_height = ul.height();
        var ul_height = 0;
        lis.each(function () {
            if ($(this).index() > count_visible_li - 1) return;
            ul_height += $(this).height();
        });
        ul.css({overflow:'hidden', height:ul_height});
        $('.more_searches_link a').click(function () {
            if (ul.height() == ul_all_height) {
                animate_height = ul_height;
                $(this).removeClass('opened');
            } else {
                animate_height = ul_all_height
                $(this).addClass('opened');
            }
            ul.animate({height:animate_height}, 500);
            return false;
        });

        $('.field-name-field-big-video  a.readmore').click(function () {
            $('.field-name-field-big-video').hide();
            return false;
        });

    });

})(jQuery);