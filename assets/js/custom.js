(function ($) {
$(function(){
	//PDF links
	$(".maincontent  a[href$='pdf']").attr('target','_blank');
    $('.portfolio-block-text, .awards-wrapper-text, .frontent-news-item').matchHeight();

    $('.stext-slider').bxSlider({
        mode: 'fade',
        controls: false,
		adaptiveHeight: true
    });

    $('.slider-front').bxSlider({
        mode: 'fade',
		pause: 7000,
        controls: false,
		adaptiveHeight: false,
		auto: true,
        pagerCustom: ".slider-front-nav"
    });

    //Testimonials tabs
	var wwidth = $(window).width();
	console.log(wwidth);
	if(wwidth>=768)
	{
		$(".customers-wrapper-title-tab").click(function() {
        $(".customers-wrapper-title-tab").removeClass("__active");
        $(this).addClass("__active");
        $(".customers-wrapper-text").hide();
        var activeTab = $(this).attr("data-tab");
        $('#'+activeTab).fadeIn();
        return false;
		});
	} else
	{
			$(".customers-wrapper-title-tab").click(function() {
        $(".customers-wrapper-title-tab").removeClass("__active");
        $(this).addClass("__active");
        $(".customers-wrapper-text").hide();
        var activeTab = $(this).attr("data-tab");
		$('#'+activeTab).insertAfter($(this));
        $('#'+activeTab).fadeIn();
        return false;
		});
	}


    // Free trial
    /*
	$('.header-links-2 .btn-full').click(function() {
        $('.freetrial').toggleClass('__active');
        return false;
    });
*/
    // search
    $('.header-search-open').click(function() {
        $('.header-search-wrapper').toggleClass('__active');
        return false;
    });
	
	//Mobile menu
	//var $menu = $("#menutop").clone();
	//$menu.attr( "id", "my-mobile-menu" );
	//$menu.removeClass('hidden-xs hidden-sm hidden-md header-menu');
	 $("#my-mobile-menu").mmenu();
	 
	 //Mobile menu select
	 $(".mobile-menu .menu-block-wrapper>ul>li:first-child").click(function() {
        $(".mobile-menu .menu-block-wrapper>ul>li:not(:first-child), .mobile-menu .menu-block-wrapper>ul>li:first-child>ul.menu").fadeToggle();
        return false;
    });
	 //Left collapsed menu
	 $('.region-leftcol ul.menu li:not(.active-trail) ul.menu').hide();
	 var allPanels = $('.region-leftcol .menu-block-wrapper>ul.menu>li>ul.menu');
	 $('.region-leftcol .menu-block-wrapper>ul.menu>li.expanded>a, .region-leftcol .menu-block-wrapper>ul.menu>li.expanded>span').click(function() {
	 $('.region-leftcol ul.menu li.expanded ').removeClass('__active active-trail');
	if($($(this).next('ul.menu')).is(':visible')) {
		$(this).next('ul.menu').slideUp();
	} else
	{
		allPanels.slideUp();
		$(this).parent().addClass('__active');
		$(this).next().slideDown();
	}
		return false;
  });
 //select styling
 
    $('select:not(#webform-component-country select)').styler({
	selectPlaceholder: 'Select'
	});
	
    $('#webform-component-country select').styler({
	selectPlaceholder: 'Country'
	});
  //top menu submenu centering
  $(".header-menu > ul.menu > li >.menu").each(function() {
	var topmenuwith = $(this).width()/2;
  	 $(this).css("margin-left","-"+topmenuwith+"px");
  });
	//Right column absolute fix
	if($('.rightcol').height())
	{
			var righcolheight = $('.rightcol').height();
			var mheight = $('.maincontent').height();
			if(mheight<righcolheight-100)
			{
				$('.maincontent').css('min-height', righcolheight+'px');
			}
			
	}

	
	 // Detect IE version
    var iev=0;
    var ieold = (/MSIE (\d+\.\d+);/.test(navigator.userAgent));
    var trident = !!navigator.userAgent.match(/Trident\/7.0/);
    var rv=navigator.userAgent.indexOf("rv:11.0");

    if (ieold) iev=new Number(RegExp.$1);
    if (navigator.appVersion.indexOf("MSIE 10") != -1) iev=10;
    if (trident&&rv!=-1) iev=11;

    // Firefox or IE 11
    if(typeof InstallTrigger !== 'undefined' || iev == 11) {
        var lastScrollTop = 0;
        $(window).on('scroll', function() {
            st = $(this).scrollTop();
            if(st < lastScrollTop) {
             $('.fixed-top').removeClass("fixed");
			      if ($(this).scrollTop() > 99) {
 					$('header').addClass("fixed");
                } else {
       					
						$('header').removeClass("fixed");
                }
            }
            else if(st > lastScrollTop) {
                  if ($(this).scrollTop() > 99) {
                    $('.fixed-top').addClass("fixed");
					$('header').removeClass("fixed");
                } else {
                    $('.fixed-top').removeClass("fixed");
					$('header').addClass("fixed");
                }
            }
            lastScrollTop = st;
        });
    }
    // Other browsers
    else {
        $('body').on('mousewheel', function(e){
            if(e.originalEvent.wheelDelta > 0) {
             $('.fixed-top').removeClass("fixed");
		
			      if ($(this).scrollTop() > 99) {
 					$('header').addClass("fixed");
                } else {
       					
						$('header').removeClass("fixed");
                }
            }
            else if(e.originalEvent.wheelDelta < 0) {
	
                   if ($(this).scrollTop() > 99) {
                    $('.fixed-top').addClass("fixed");
					$('header').removeClass("fixed");
                } else {
                    $('.fixed-top').removeClass("fixed");
					$('header').addClass("fixed");
                }
            }
        });
    }

	//Top form get trial
	
	$('#topformsubmit').click(function() {
		var emailval  = $(".topformload-1st .form-item-mail .form-text").val();
		var mailvalid = emailval.length;
		if(mailvalid < 4) {
				$(".topformload-1st .form-item-mail .form-text").addClass("error");
		} else {
			$('.topformload-1st').hide();
			$('#webform-component-corporate-email .form-text').val(emailval);
			$('#webform-component-corporate-email').addClass("goodmail");
			$('.fixed-top').addClass('megafixed');
			$('body').addClass('bodyfix');
			$('#topformload').fadeToggle();
		}
	
        return false;
    });
	$('.topformload-close').click(function() {
	$('#topformload').hide();
        $('.topformload-1st').fadeToggle();
			$(".topformload-1st .form-item-mail .form-text").removeClass("error");
		$('#webform-component-corporate-email').removeClass("goodmail");
		$('.fixed-top').removeClass('megafixed');
		$('body').removeClass('bodyfix');
        return false;
    });
});

}(jQuery));